:PROPERTIES:
:ID:       66ee74d1-76df-4f92-9ce8-3712a50eb7b5
:END:

#+title: Integrated Linked List
#+date: 2024-07-23
#+draft: false
#+hugo_auto_set_lastmod: t
#+garden_tags: c++ programming
#+hugo_base_dir: ../
#+hugo_section: garden
#+hugo_custom_front_matter: :summary "Linked list" :status "growing" :garden_tags ["c++" "programming"]

* Simple linked list

I have a goal of creating an STL-friendly linked list. For now, I have
the following.

- The class =LinkedList= has a nested class =Node=. This is a design
  choice, since I do not want the user to have access to =Node=
  objects.
- The =iterator= class is also nested inside =LinkedList=. For it to
  work properly, we need to mark the type of iterator we are using. A
  [[https://cplusplus.com/reference/iterator/ForwardIterator/4][=forward_iterator=]] in this case.
- Having the iterator allow us to use a =for-range= loop with our
  =LinkedList= collection.
- Overloading, equals and diferent operators allows to
  use =std::find= from the =algorithm= header. 

#+begin_src cpp
  #include <iostream>
  #include <initializer_list>
  #include <iterator>
  #include <algorithm>

  class LinkedList
  {
  public:
      LinkedList() : head(nullptr), _sz(0) {};
      LinkedList(std::initializer_list<int> list);
      bool empty() const { return head == nullptr; };
      std::size_t size() const { return _sz; };
      void push_front(int val);
      ~LinkedList();

  private:
      class Node
      {
      public:
	  Node() : _value(0), next(nullptr) {}
	  Node(int val) : _value(val), next(nullptr) {}
	  int value() const { return _value; }

      private:
	  int _value;
	  Node *next;
	  friend class LinkedList;
      };
      Node *head;
      std::size_t _sz;

  public:
      class iterator : public std::iterator<std::forward_iterator_tag, int>
      {
      private:
	  Node *ptr;

      public:
	  iterator(Node *p) : ptr(p) {}
	  int &operator*() { return ptr->_value; }
	  iterator &operator++()
	  {
	      ptr = ptr->next;
	      return *this;
	  }
	  bool operator==(const iterator &other) const
	  {
	      return ptr == other.ptr;
	  }
	  bool operator!=(const iterator &other) const
	  {
	      return not(*this == other);
	  }
      };
      iterator begin() { return iterator(this->head); }
      iterator end() { return iterator(nullptr); }
  };

  LinkedList::LinkedList(std::initializer_list<int> list)
  {
      this->head = nullptr;
      this->_sz = 0;
      /* Only the free function-version of rbegin and rend are available */
      for (auto x = rbegin(list); x != rend(list); ++x)
      {
	  this->push_front(*x);
      }
  }

  void LinkedList::push_front(int val)
  {
      Node *new_node = new Node(val);
      new_node->next = head;
      head = new_node;
      ++(this->_sz);
  }

  LinkedList::~LinkedList()
  {
      Node *p = head;
      while (p != nullptr)
      {
	  p = p->next;
	  delete head;
	  head = p;
      }
  }

  int main(void)
  {
      LinkedList v = {0, 10, 20, 30, 40, 50};
      for (const auto &x : v)
      {
	  std::cout << x << ' ';
      }
      std::cout << '\n';
      LinkedList::iterator target = std::find(v.begin(), v.end(), 30);
      std::cout << "Target is " << *target << '\n';
      return 0;
  }
#+end_src

#+RESULTS:
|      0 | 10 | 20 | 30 | 40 | 50 |
| Target | is | 30 |    |    |    |

* Somethings I am thinking about adding

+ How to add the free-function version of begin and end?
+ Overload more operations to be able to use other stl algorithms

