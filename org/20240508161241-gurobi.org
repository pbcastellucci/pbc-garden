:PROPERTIES:
:ID:       3a1f89cc-4400-47fa-9f2f-af2b3b9b138e
:END:
#+title: Gurobi utils
#+date: 2024-05-08
#+draft: false
#+hugo_auto_set_lastmod: t
#+garden_tags: gurobi install python c++
#+hugo_base_dir: ../
#+hugo_section: garden
#+hugo_custom_front_matter: :summary "Gurobi useful installation tips" :status "growing" :garden_tags ["gurobi" "install" "python" "c++"]

* Python installation

** Using pip

It seems like the default method of installation now should be using pip
#+begin_src shell
  pip install gurobipy
#+end_src
It comes with a trial license, that let the user run small instances.

** Without pip (I think this is deprecated know)

At GUROBI_HOME folder:
#+begin_src bash
  sudo python3 setup.py install
#+end_src

** Setting up the paths

Add to .bashrc file, which is in your home directory:

#+begin_src bash
# Change the folder to the appropriate version:
export GUROBI_HOME="/opt/gurobi951/linux64" 
export PATH="${PATH}:${GUROBI_HOME}/bin"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${GUROBI_HOME}/lib"
export GRB_LICENSE_FILE="/home/your_home/gurobi.lic"
#+end_src

* Generating the libgurobi (for C++ use)

#+begin_src bash
cd /opt/gurobi912/linux64/src/build  
make  
cp libgurobi_c++.a ../../lib/
#+end_src

** Compiling with Makefile

#+begin_src bash
CC = g++
FLAGS = -g -Wall -Wextra
GRBPATH = /opt/gurobi912

GUROBI_LIBS = -L$(GRBPATH)/linux64/lib
GUROBI_FLAGS = -lgurobi_c++ -lgurobi91

all:
	$(CC) $(FLAGS) TSPSolverGurobi.cpp -o tsp_solver_grb -I$(GRBPATH)/linux64/include $(GUROBI_LIBS) $(GUROBI_FLAGS)

clean:
	rm *.o || true
	rm exec || true
	rm *.*~  || true                                              
#+end_src

* License

https://www.gurobi.com/downloads/end-user-license-agreement-academic/

* Why is it growing?
- How to setup VSCode?
