:PROPERTIES:
:ID:       8c2cb905-851b-4644-b492-40ac873615e0
:END:

#+title: Hugo and ox-hugo
#+date: 2024-02-04
#+draft: false
#+hugo_auto_set_lastmod: t
#+garden_tags: emacs hugo
#+hugo_base_dir: ../
#+hugo_section: garden
#+hugo_custom_front_matter: :summary "Quick starting ox-hugo" :status "growing" :garden_tags ["emacs" "hugo"]


* Hugo quick-start

For now, I am using a combination of Hugo, ox-hugo and Org-roam for my
garden. It seems to be working.

** Install

#+begin_src shell
  sudo snap install hugo
#+end_src

** First digital garden

We need to start by doing the basic Hugo setup.

#+begin_src shell
hugo new site quickstart
cd quickstart
git init
#+end_src

I found a digital garden theme that seems appropriate [[https://themes.gohugo.io/themes/hugo-digital-garden-theme/][here]]. 

#+begin_src shell
git submodule add https://github.com/paulmartins/hugo-digital-garden-theme.git themes/hugo-digital-garden-theme
#+end_src

Then, I copied the content of
=/themes/hugo-digital-garden-theme/exampleSite/= into =HUGO_BASE_DIR=
and changed a line =config.toml=

#+begin_src shell
  themesdir = "./themes"
#+end_src

* =HUGO_BASE_DIR= and other metadata

We must setup the =HUGO_BASE_DIR= variable. Markdown files will be
  exported to =~/HUGO_BASE_DIR/content/<HUGO_SECTION>/=. One way to do
  that is using the metadata in the files. For this page, for example,
  I have

#+begin_src shell
  ,#+title: Hugo and ox-hugo
  ,#+date: 2024-02-04
  ,#+hugo_auto_set_lastmod: t
  ,#+draft: false
  ,#+hugo_tags: emacs hugo
  ,#+hugo_base_dir: ../garden
  ,#+hugo_section: garden
  ,#+hugo_custom_front_matter: :summary "Quick starting ox-hugo" :status "seeding"
#+end_src

* Publishing

I use command

#+begin_src shell
  hugo
#+end_src

to generate the =public= folder locally and just push it to gitlab
with =.gitlab-ci.yml= file like the following.

#+begin_src shell
  pages: 
    stage: deploy
    script:
      - echo "Generating page..."
    artifacts:
      paths:
	- public
    only:
      - main
#+end_src

* Why is it growing?

- I feel like there is a lot I do not know about the possibilities
  yet: custom CSS, archetypes, sections...

* Links and references

+ [[https://ox-hugo.scripter.co/][The documentation of ox-hugo]].
